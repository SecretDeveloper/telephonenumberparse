﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TelephoneNumberParser;



namespace TelephoneNumberParseTests
{
    [TestClass]
    public class ParsingTests
    {
        [TestMethod]
        public void CanParser_DetermineIfANumberIsInternational()
        {
            var samples = new Dictionary<string, bool>();
            samples.Add("ooOO23213123123", false); // invalid international code -- 0
            samples.Add("003531234567890", true);
            samples.Add("+353512345", true);
            samples.Add("+1234 Extension 999", false); // too short <8
            samples.Add("+1234567 Extension 999", true);
                // potentially valid, shortest known international number is 7 digits long
            samples.Add("+8234567 Extension 999", false); // invalid international code "8"

            samples.Add("  123  456  7789  0000o", false);
            samples.Add("+sfkllkj()!:@~|SFR", false);
            samples.Add("1111111111111", false);
            samples.Add("(087)9991111", false);
            samples.Add("1.1.1.1..1", false);

            var telephoneNumber = new TelephoneNumber();
            foreach (var sample in samples)
            {
                telephoneNumber.ParseNumber(sample.Key); // parse the number

                var actual = telephoneNumber.IsInternational;
                var expected = sample.Value;

                if (actual != expected)
                {
                    Assert.Fail(
                        string.Format(
                            "Parsing '{0}' resulted in an IsInternational value of ({1}) when ({2}) was expected.",
                            sample.Key, actual, expected));
                }
            }
        }

        [TestMethod]
        public void CanParser_DetermineIfANumberIsEmergency()
        {
            var samples = new Dictionary<string, bool>();
            samples.Add("999", true);
            samples.Add("+999", true);
            samples.Add("112", true);
            samples.Add("+123 Extension 999", true); // too short <8
            samples.Add("065", true);

            samples.Add("  123  456  7789  0000o", false);
            samples.Add("+sfkllkj()!:@~|SFR", false);
            samples.Add("1111111111111", false);
            samples.Add("(087)9991111", false);
            samples.Add("1.1.1.1..1", false);
            samples.Add("999/112", false);

            var telephoneNumber = new TelephoneNumber();
            foreach (var sample in samples)
            {
                telephoneNumber.ParseNumber(sample.Key); // parse the number

                var actual = telephoneNumber.IsEmergency;
                var expected = sample.Value;

                if (actual != expected)
                {
                    Assert.Fail(
                        string.Format(
                            "Parsing '{0}' resulted in an IsEmergency value of ({1}) when ({2}) was expected.",
                            sample.Key, actual, expected));
                }
            }
        }

        [TestMethod]
        public void CanParser_CleanStrings()
        {
            var samples = new Dictionary<string, string>();
            samples.Add("ooOO", "0000"); // Leadin 00 should be changed to +
            samples.Add("1234567890", "1234567890");
            samples.Add("  123  456  7789  0000o", "123456778900000");
            samples.Add("+sfkllkj()!:@~|SFR", "+"); // TODO - not sure if we should return "" here instead
            samples.Add("1111111111111", "1111111111111");
            samples.Add("(087)9991111", "0879991111");
            samples.Add("+353512345", "+353512345");
            samples.Add("+099912 Extension 999", "+099912"); // TODO - Confirm this is how we want it to behave.
            samples.Add("1.1.1.1..1", "11111");


            var telephoneNumber = new TelephoneNumber();
            foreach (var sample in samples)
            {
                var actual = telephoneNumber.Clean(sample.Key);
                var expected = sample.Value;

                if (actual != expected)
                {
                    Assert.Fail(string.Format("Parsing '{0}' resulted in ({1}) when ({2}) was expected.", sample.Key,
                                              actual, expected));
                }
            }
        }

        [TestMethod]
        public void CanParserDetect_IsMobile()
        {
            // create some sample numbers with the result we expect from parsing.
            var samples = new Dictionary<string, TelephoneNumber>();
            //Valid mobiles 
            samples.Add("(083) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("(085) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("(086) 315 8000",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("(087) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("(089) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});

            samples.Add("0831607764", new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0851607764", new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0861607764", new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0871607764", new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0891607764", new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});

            samples.Add("+353832039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+353852039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+353862039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+353872039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+353892039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            //UK
            samples.Add("+44-7769-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+447769725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0044 7533 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+447533226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            //France
            samples.Add("+33-6769-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+336769725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0033 6533 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+336533226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+33-7769-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+337769725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0033 7533 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+337533226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            //Germany
            samples.Add("+49-1519-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+491529725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0049 1553 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+491573226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+49-1599-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+491609725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("0049 1623 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});
            samples.Add("+491703226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = true});





            //Invalid Mobiles
            samples.Add("(013) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("(025) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("(036) 315 8000",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("(047) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("(059) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("(069) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("(079) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("(099) 160 7764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});

            samples.Add("0131607764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0251607764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0361607764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0471607764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0591607764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0691607764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0791607764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0991607764",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});

            samples.Add("+353132039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+353252039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+353362039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+353472039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+353592039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+353692039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+353792039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+353992039122",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            //UK
            samples.Add("+44-1769-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+442769725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0044 3533 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+444533226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            //France
            samples.Add("+33-1769-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+332769725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0033 3533 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+334533226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+33-5769-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+338769725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0033 9533 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+339533226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            //Germany
            samples.Add("+49-1809-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+492769725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0049 3533 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+494533226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+49-5769-725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+498769725068",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("0049 9533 226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});
            samples.Add("+499533226110",
                        new TelephoneNumber() {Dial = "", Display = "", Information = "", IsMobile = false});


            foreach (var sample in samples)
            {
                var actual = new TelephoneNumber(sample.Key);
                var expected = sample.Value;

                if (actual.IsMobile != expected.IsMobile)
                {
                    Assert.Fail(string.Format("Parsing '{0}' resulted in IsMobile ({1}) when ({2}) was expected.",
                                              sample.Key, actual.IsMobile.ToString(), expected.IsMobile.ToString()));
                }
            }

        }

        [TestMethod]
        public void CanParser_DisplayNumbersCorrectly()
        {
            // create some sample numbers with the result we expect from parsing.
            var samples = new Dictionary<string, TelephoneNumber>();
            //Valid mobiles 
            samples.Add("(083) 160 7764",
                        new TelephoneNumber()
                            {Dial = "+353831607764", Display = "+353 (0)83 160 7764", Information = "", IsMobile = true});

            samples.Add("0831607764", new TelephoneNumber() { Dial = "+353831607764", Display = "+353 (0)83 160 7764", Information = "", IsMobile = true });
            samples.Add("+353832039122",new TelephoneNumber() { Dial = "+353832039122", Display = "+353 (0)83 203 9122", Information = "", IsMobile = true });
            samples.Add("(051) 7766872", new TelephoneNumber() { Dial = "+353517766872", Display = "+353 (0)51 776 6872", Information = "", IsMobile = true });
            //samples.Add("0131607764", new TelephoneNumber() { Dial = "+0131607764", Display = "+353 (0)1 316 07764", Information = "", IsMobile = true });  // This one is trickey and requires knowledge of regional dialling codes.
            samples.Add("041 291 2121", new TelephoneNumber() { Dial = "+353412912121", Display = "+353 (0)41 291 2121", Information = "", IsMobile = true });
            samples.Add("(043) 937 0200 [Ext 2930]", new TelephoneNumber() { Dial = "+353439370200", Display = "+353 (0)43 937 0200", Information = "[Ext 2930]", IsMobile = true });
            
            //UK
            samples.Add("+44-7769-725068",
                        new TelephoneNumber()
                            {Dial = "+447769725068", Display = "+44 (0)7769 725068", Information = "", IsMobile = true});
            
            samples.Add("+447769725068",
                        new TelephoneNumber()
                            {Dial = "+447769725068", Display = "+44 (0)7769 725068", Information = "", IsMobile = true});
            
            

            foreach (var sample in samples)
            {
                var actual = new TelephoneNumber(sample.Key);
                var expected = sample.Value;

                if (expected.Display != actual.Display)
                {
                    Assert.Fail(string.Format("Parsing '{0}' resulted in Display ({1}) when ({2}) was expected.",
                                              sample.Key, actual.Display, expected.Display));
                }

                if (expected.Dial != actual.Dial)
                {

                    Assert.Fail(string.Format("Parsing '{0}' resulted in Dial ({1}) when ({2}) was expected.",
                                                  sample.Key, actual.Dial, expected.Dial));
                }               
            }
        }

        [TestMethod]
        public void CanParser_LoadInformationField()
        {
            // create some sample numbers with the result we expect from parsing.
            var samples = new Dictionary<string, TelephoneNumber>();
            //Valid mobiles 
            samples.Add("(042) 937 0200 [Ext 2930]",
                        new TelephoneNumber() { Dial = "", Display = "", Information = "[Ext 2930]", IsMobile = false });
            samples.Add("(042) 937 0200 [Ext. 2336]",
                        new TelephoneNumber() { Dial = "", Display = "", Information = "[Ext. 2336]", IsMobile = false });
            samples.Add("+35386315 8000 Then ask for Tommy O'Brien for details.",
                        new TelephoneNumber() { Dial = "", Display = "", Information = "Then ask for Tommy O'Brien for details.", IsMobile = true });

            foreach (var sample in samples)
            {
                var actual = new TelephoneNumber(sample.Key);
                var expected = sample.Value;

                if (expected.Information != actual.Information)
                {
                    Assert.Fail(string.Format("Parsing '{0}' resulted in Information ({1}) when ({2}) was expected.",
                                              sample.Key, actual.Information, expected.Information));
                }
            }
        }
    }
}
