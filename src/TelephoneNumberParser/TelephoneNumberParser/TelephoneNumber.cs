﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelephoneNumberParser
{
    public class TelephoneNumber
    {
        private const string AllowedCharacters = "+0123456789";
        private const string InternationalPrefixDigits = "0,1,9";


        /// <summary>
        /// Contains the string representation of how the number should be displayed to the User.
        /// </summary>
        public string Display { get; set; }
        /// <summary>
        /// Contains the a string representing the number as it should be dialled e.g. +353879991111
        /// </summary>
        public string Dial { get; set; }
        /// <summary>
        /// Contains additional dialling information required to make contact e.g Extension information.
        /// </summary>
        public string Information { get; set; }
        /// <summary>
        /// Number has been validated externally using a validation SMS or other method.
        /// </summary>
        public bool IsValid { get; set; }
        /// <summary>
        /// True if the number represents a mobile or cell phone number.
        /// </summary>
        public bool IsMobile { get; set; }
        /// <summary>
        /// True if the number represents an Emergency service number e.g. 911, 999, 112 etc.
        /// </summary>
        public bool IsEmergency { get; set; }
        /// <summary>
        /// True if the number represents an internationally diallable number e.g. +353879991111
        /// </summary>
        public bool IsInternational { get; set; }

        private void Reset()
        {
            this.Dial = string.Empty;
            this.Display = string.Empty;
            this.Information = string.Empty;
            this.IsEmergency = false;
            this.IsInternational = false;
            this.IsMobile = false;
            this.IsValid = false;
        }

        private bool DetermineIfNumberInternational(string telephoneNumber)
        {
            // Too short to be international - shortest int number is 7 digits (+1 for the + symbol).
            if (telephoneNumber.Length < 8)
                return false;

            // Starts with + -- probably international
            if (telephoneNumber[0].Equals('+'))
            {
                // international dialling codes -- check the first one after the 00 is one of these.
                if ("1,2,3,4,5".Contains(telephoneNumber.Substring(1, 1)))
                    return true;                
            }

            // Starts with 00 -- probably international
            if (telephoneNumber[0].Equals('0') && telephoneNumber[1].Equals('0'))
            {
                // international dialling codes -- check the first one after the 00 is one of these.
                if ("1,2,3,4,5".Contains(telephoneNumber.Substring(2, 1)))
                    return true;
            }

            return false;
        }

        private bool DetermineIfNumberIsEmergency(string telephoneNumber)
        {
            if(telephoneNumber.Length <2)
                return false;

            if (telephoneNumber.Length > 3 && telephoneNumber[0] != '+')
                return false;

            char firstDigit = telephoneNumber[0] == '+' ? telephoneNumber[1] : telephoneNumber[0];

            if (InternationalPrefixDigits.Contains(firstDigit))
                return true;

            return false;
        }

        private bool DetermineIfNumberIsMobile(string telephoneNumber)
        {
            if (telephoneNumber.Length < 10)
                return false;


            switch (telephoneNumber.Substring(0, 3))
            {
                case "+44": // UK
                    if (telephoneNumber[3] == '7')
                        return true;
                    break;
                case "+35": // Ireland
                    if (telephoneNumber[4] == '8')
                        return true;
                    break;
                case "+33": // France
                    if (telephoneNumber[3] == '6' || telephoneNumber[3] == '7')
                        return true;
                    break;
                case "+49": // Germany
                    if ("151,152,155,157,159,160,162,163,170,171,172,173,174,175,176,177,178,179".Contains(telephoneNumber.Substring(3, 3)))
                        return true;
                    break;
                default:
                    return false;
            }

            // Hack to catch non international numbers -- assuming ireland and check for 8 as first non 0 digit
            string prefix = telephoneNumber.FirstOrDefault(x => x != '0').ToString();
            if (prefix == "8")
                return true;

            return false;
        }

        private string FormatNumberForDisplay(string telephoneNumber)
        {
           
            if (this.IsEmergency)
                return telephoneNumber;  // no formatting.

            if (telephoneNumber.Length < 6)
                return telephoneNumber; // too short -- just dump it back

            int firstGroupingCount = 3;
            int secondGroupingCount = 5;
            int thirdGroupingCount = 8;
            int prefixLength = 0;
            if(telephoneNumber[0] == '+')  // number should be in the format +xxxxxxx
            {   
                switch (telephoneNumber.Substring(0,3))
                {
                    case "+44":
                    case "+49":
                    case "+33":
                        prefixLength = 2;
                        firstGroupingCount = 3;
                        secondGroupingCount = 6;
                        thirdGroupingCount = 0;
                        break;

                    case "+35":
                        prefixLength = 4;
                        firstGroupingCount = 4;
                        break;
                }
                //return telephoneNumber.Substring(0, prefixLength) + " (0) " + telephoneNumber.Substring(prefixLength);
            }

            StringBuilder formatted = new StringBuilder();
            bool first = true;            
            int groupCount = 0;

            foreach (char ch in telephoneNumber)
            {
                if (prefixLength > 0 && formatted.Length > prefixLength)
                {
                    if (first)
                    {
                        formatted.Append("(0)");
                        if (ch != '0')
                            formatted.Append(ch);

                        first = false;
                        continue;
                    }
                }


                formatted.Append(ch);

                groupCount++;
                if (firstGroupingCount>0 && groupCount == firstGroupingCount)
                {
                    formatted.Append(" ");                    
                }

                if (secondGroupingCount > 0 && groupCount == secondGroupingCount)
                {
                    formatted.Append(" ");                    
                }

                if (thirdGroupingCount > 0 && groupCount == thirdGroupingCount)
                {
                    formatted.Append(" ");
                }

            }
            return formatted.ToString();
        }

        public TelephoneNumber()
        {
            Reset();
        }

        public TelephoneNumber(string telephoneNumber)
        {
            Reset();
            ParseNumber(telephoneNumber);
        }

        public void ParseNumber(string telephoneNumber)
        {
            Reset();
            //Clean any invalid characters.
            string cleaned = Clean(telephoneNumber);
            this.IsInternational = DetermineIfNumberInternational(cleaned); // determine if cleaned number is international
            this.IsEmergency = DetermineIfNumberIsEmergency(cleaned);
                        
            cleaned = PrependPrefix(cleaned); // + or +353 as appropriate

            this.IsMobile = DetermineIfNumberIsMobile(cleaned);

            this.Dial = cleaned;
            this.Display = FormatNumberForDisplay(cleaned);
        }

        private string PrependPrefix(string telephoneNumber)
        {            
            // strip leading 00 and replace with +
            if (telephoneNumber[0] == '0' && telephoneNumber[1] == '0')
            {
                telephoneNumber = "+" + telephoneNumber.Substring(2);
            }
            else
            {
                // assume its an IRE number, append +353 and strip any leading 0.
                if (telephoneNumber[0] == '0')
                {
                    telephoneNumber = "+353" + telephoneNumber.Substring(1);
                }
            }

            return telephoneNumber;
        }
        
        /// <summary>
        /// Strip out any non-numeric characters and get a string of integers
        /// </summary>
        /// <param name="telephoneNumber"></param>
        /// <returns></returns>
        public string Clean(string telephoneNumber)
        {
            if (telephoneNumber.Length <= 3)
                return telephoneNumber;

            int invalidCharCount = 0;
            int charIndex = -1;
            int invalidCharStartIndex = -1;
            StringBuilder cleaned = new StringBuilder(15);

            foreach(char ch in telephoneNumber)
            {
                charIndex++; // track where we are in the string
                if (AllowedCharacters.Contains(ch))
                {
                    cleaned.Append(ch);
                    invalidCharCount = 0; // reset as we have received a valid character.
                    invalidCharStartIndex = -1; //reset also
                    continue;
                }
                if(ch == 'o' || ch == 'O')
                {
                    cleaned.Append("0"); // replace the letter o or O with 0 as it is a common typo.
                    continue;
                }


                invalidCharCount++;
                if (ch != ' ')// dont count ' ' as the start of invalid strings when placing them in information field.
                    invalidCharStartIndex = charIndex -1;

                if(invalidCharCount == 3) // this is our third invalid char -- drop rest of string -- "000000 Ext 822" > "000000"
                {
                    if (invalidCharStartIndex > -1 && invalidCharStartIndex < telephoneNumber.Length)
                        this.Information = telephoneNumber.Substring(invalidCharStartIndex);
                    break;
                }
            }

            return cleaned.ToString();
        }

    }

    /// <summary>
    /// Error handling helper class
    /// </summary>
    public class Result
    {
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
        public Result()
        {
            this.Success = true;
            this.Messages = new List<string>();
        }
    }
}
